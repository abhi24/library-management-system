import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  token: string;
  constructor(private router: Router) {}
  signUpUser(name: string, pass: string) {
    firebase.auth().createUserWithEmailAndPassword(name, pass);
}


  signInUser(name: string, pass: string) {
    firebase.auth().signInWithEmailAndPassword(name, pass)
      .then(
        response => {
          this.router.navigate(['/']);
          firebase.auth().currentUser.getToken()
            .then(
              (token: string) => this.token = token
            );
        }
      );
  }

  getToken() {
    firebase.auth().currentUser.getToken()
      .then(
        (token: string) => this.token = token
      );
    return this.token;
  }

  isAuthenticated() {
    return this.token != null;
  }
}
