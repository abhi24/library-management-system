import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authSer: AuthService) { }

  ngOnInit() {
  }
  onSignup(form: NgForm) {
    this.authSer.signUpUser(form.value.email,  form.value.password);
  }
}
