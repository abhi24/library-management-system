import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'testfilter',
  pure: false
})
export class TestPipe implements PipeTransform {

  transform(value: any, filterString: any): any {
    if (filterString % 2 === 0) {
      return true;
    } else  {
      return false;
    }
  }
}
