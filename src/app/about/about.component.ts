import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  filteredSta= new Date(1988, 3, 15);
  toggle= true;
  arrays = ['abc', 'asdf', 'fdsfe'];

  @ViewChild('f') aboutForm = NgForm;

  constructor() { }

  get format()   { return this.toggle ? 'shortDate' : 'fullDate'; }
  toggleFormat() { this.toggle = !this.toggle; }


  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    console.log(f.value);
  }

}
