import {SharedPeople} from './sharedpeople.model';

export class Carel {
  public name: string;
  public date: Date;
  public endDate: Date;
  public category: string;
  public ingredients: SharedPeople[];

  constructor(name: string, date: Date, endDate: Date, category: string, ingredients: SharedPeople[]) {
    this.name = name;
    this.date = date;
    this.endDate = endDate;
    this.category = category;
    this.ingredients = ingredients;
  }
}
