import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {DataStorageService} from '../../shared/data.storage.service';
import {Response} from '@angular/http';
import {BooksService} from '../../books/books.service';


@Component({
  selector: 'app-study-carrel',
  templateUrl: './study-carrel.component.html',
  styleUrls: ['./study-carrel.component.css']
})
export class StudyCarrelComponent implements OnInit {

  enum_details = [{name: 'MS'}, {name: 'PHD'}, {name: 'UNDERGRAD'}];
  radioSelected: any;
  carelForm: FormGroup;
  today = new Date().toJSON().split('T')[0];

  constructor(private dsSer: DataStorageService, private bookSer: BooksService) { }

  ngOnInit() {
    this.initForm();
  }


  onClear() {
   this.carelForm.reset();
  }

  onAddIngredient() {
    (<FormArray>this.carelForm.get('names')).push(
      new FormGroup({
        'nameIn': new FormControl(null, Validators.required),
        'age': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ])
      })
    );
  }
  onDeleteIngredient(index: number) {
    (<FormArray>this.carelForm.get('names')).removeAt(index);
  }

  private initForm() {

    const namess = new FormArray([]);

    namess.push(
      new FormGroup({
        'nameIn': new FormControl(null, Validators.required),
        'age': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ])
      })
    );

    this.carelForm = new FormGroup({
    'name': new FormControl(null, Validators.required),
      'date': new FormControl(null, Validators.required),
      'endDate': new FormControl(null, Validators.required),
      'category': new FormControl(null, Validators.required),
      'names': namess
   });
  }


  onSubmit() {

     this.bookSer.addCarel(this.carelForm.value);

    this.dsSer.addCarel1()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );


  }
}
