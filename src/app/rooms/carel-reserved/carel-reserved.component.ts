import { Component, OnInit } from '@angular/core';
import {BooksService} from '../../books/books.service';
import {Carel} from '../carel.model';
import {DataStorageService} from '../../shared/data.storage.service';
import {FormArray} from '@angular/forms';

@Component({
  selector: 'app-carel-reserved',
  templateUrl: './carel-reserved.component.html',
  styleUrls: ['./carel-reserved.component.css']
})
export class CarelReservedComponent implements OnInit {
  carels: Carel[];
  car= [] ;
   s = [];

  constructor(private bookSer: BooksService, private dsSer: DataStorageService) {
  }

  ngOnInit() {
    this.carels = this.bookSer.getCarel();
    console.log(this.carels);
  }
}
