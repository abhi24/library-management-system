export class Library {
  public name: string;
  public lat: number;
  public lon: number;

  constructor(name: string, lat: number, lon: number) {
    this.name = name;
    this.lat = lat;
    this.lon = lon;

  }
}
