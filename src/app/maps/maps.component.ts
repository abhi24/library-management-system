import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MapsAPILoader} from '@agm/core';
import {DataStorageService} from '../shared/data.storage.service';
import {Library} from './library.module';
import {BooksService} from '../books/books.service';
import {Response} from '@angular/http';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
  `],
})

export class MapsComponent implements OnInit {

  libraryform: FormGroup;
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private dsSer: DataStorageService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private bookSer: BooksService
  ) {}

  ngOnInit() {
    this.zoom = 14;
    this.latitude = 28.6229;
    this.longitude = 77.089;
    this.init();
    this.searchControl = new FormControl();

    this.setCurrentPosition();

    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  onSubmit() {
    const name = this.libraryform.get('shopname').value;
    this.bookSer.addNewLib(new Library(name , this.latitude, this.longitude));

    this.dsSer.addLib()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );
  }

  init() {
    this.libraryform = new FormGroup({
      'shopname': new FormControl(null)
    });
  }
}
