import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {MapsAPILoader} from '@agm/core';
import {DataStorageService} from '../../shared/data.storage.service';
import {Library} from '../library.module';
import {BooksService} from '../../books/books.service';

@Component({
  selector: 'app-search-lib',
  templateUrl: './search-lib.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
  `],
})
export class SearchLibComponent implements OnInit {

  libDataArray = [];
  libDataArray1 = ['Vestal', 'Endicott', 'BGM-Aiport Road'];
  locFound = false;
  searchData: Library[];
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  place: string;
  types = [ '1-3km', '3-6km', '6-10km' ];
  order = {
  type: '1-3km',
  name: 'some name'
};
  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private dsSer: DataStorageService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone, private bookSer: BooksService
  ) {}
  ngOnInit() {
  //  this.dsSer.getlibraryData();
    this.searchData = this.bookSer.getLibrary();

    this.zoom = 12;
    this.latitude = 28.621899;
    this.longitude = 77.08783849999998;
    this.searchControl = new FormControl();

    this.setCurrentPosition();

    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }


  onSubmit() {


    console.log('lll');
    for (const l1 of this.searchData) {
      console.log('hi');
      if (this.latitude - l1.lat < 0.024    && this.longitude - l1.lon < 0.024 &&
        this.latitude - l1.lat > -0.024    && this.longitude - l1.lon > -0.024 && (this.order.type === '1-3km')) {
        this.libDataArray.push(l1.name);
      } else if (this.latitude - l1.lat < 0.040    && this.longitude - l1.lon < 0.040 &&
        this.latitude - l1.lat > -0.040    && this.longitude - l1.lon > -0.040 && (this.order.type === '3-6km')) {
        this.place = l1.name;
        this.libDataArray.push(l1.name);
      } else if (this.latitude - l1.lat < 0.060    && this.longitude - l1.lon < 0.060
        && this.latitude - l1.lat > -0.060    && this.longitude - l1.lon > -0.060 && (this.order.type === '6-10km')) {
        this.place = l1.name;
        this.libDataArray.push(l1.name);
      } else {
        this.locFound = true;
      }
    }
  }

  callType(value) {
    this.order.type = value;
  }



}
