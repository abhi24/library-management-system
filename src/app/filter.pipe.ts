import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {


  transform(value: any, filterString: string): any {
    if (value.length === 0) {
      return value;
    }
    const resultArray = [];
    for (const l of value) {
      if (l.name === filterString) {
          resultArray.push(l);
      }
    }
    return resultArray;
  }

}
