import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  name = '';
  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyAD0t4fhDqeTfJlhHlZVFqwuaHxEB4raoQ',
      authDomain: 'abhi-http.firebaseapp.com',
    });
  }

}
