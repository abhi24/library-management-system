import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {DataStorageService} from '../shared/data.storage.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private dsSer: DataStorageService, private authSer: AuthService) { }

  ngOnInit() {
  }
  onBookListClicked() {
    this.router.navigate(['books'], {relativeTo: this.route});
  }
  searchBook() {
    this.router.navigate(['searchlist'], {relativeTo: this.route});
  }

  returnBook() {
    this.router.navigate(['returnBook'], {relativeTo: this.route});
  }
  onHomePressed() {
    this.router.navigate(['/'], {relativeTo: this.route});
  }
  onSave() {
    this.dsSer.addCarel1().subscribe(
      (response: Response) => {

      }
    );
    this.dsSer.addLib().subscribe(
      (response: Response) => {

      }
    );
    this.dsSer.storeBooks()
      .subscribe(
        (response: Response) => {
        }
      );
  }
  onFetchData() {
    this.dsSer.getBook();
    this.dsSer.getCarel();
    this.dsSer.getlibraryData();
  }
  carelReserved() {
    this.router.navigate(['carel-reserved'], {relativeTo: this.route});

  }
  onStudyCarrel() {
    this.router.navigate(['study-carrel'], {relativeTo: this.route});
  }

}
