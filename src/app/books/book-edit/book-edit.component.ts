import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BooksService} from '../books.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {
  bookEditForm: FormGroup;
  id: number;
  editMode = false;

  constructor(private route: ActivatedRoute,
              private bookService: BooksService,
              private router: Router ) { }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.init();
      }
    );
  }

  onSubmit() {
    if (this.editMode) {
      this.bookService.updateBook(this.id, this.bookEditForm.value);
    } else {
      console.log('Values are: ' + this.bookEditForm.value);
      this.bookService.addnewBook(this.bookEditForm.value);
    }
  }

  public init() {
    let bookName = '';
    let bookCost = null;
    let bookQuantity = null;
    let bookimage = '';

    if (this.editMode) {

      const bookfromId = this.bookService.getBookfromIndex(this.id);
      bookName = bookfromId.name;
      bookCost = bookfromId.cost;
      bookQuantity = bookfromId.quantity;
      bookimage = bookfromId.image;
    }

    this.bookEditForm = new FormGroup({
      'name': new FormControl(bookName),
      'quantity': new FormControl(bookQuantity),
      'cost': new FormControl(bookCost),
      'image': new FormControl(bookimage),

    });
  }

}
