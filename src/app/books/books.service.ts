import {Book} from './books.model';
import {Subject} from 'rxjs/Subject';
import {Carel} from '../rooms/carel.model';
import {Library} from '../maps/library.module';

export class BooksService {
    in: number;
  bookSelected = new Subject<Book[]>();
  carelSelected = new Subject<Carel[]>();
  LibSelected = new Subject<Library[]>();

  public libr: Library []= [];
  public carel: Carel []= [];

  public books: Book [] = [
    new Book('Java', 100 ,
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYVMhPcicSHp-URckD_9z6kFANqtsuUOIVuv8vMgXdGW_2_o_umQ',
      0),
    new Book('HeadFirst', 200 , 'http://wickedlysmart.com/hfhtmlcss/chapter9/lounge/images/logo.gif', 5),

  ];

  getBooks() {
    return this.books.slice();
  }

  getLibrary() {
    return this.libr.slice();
  }

  getCarel() {
    return this.carel.slice();
  }
  getCarelfromIndex(index: number) {
    return this.carel[index];
  }

  addNewLib(lib: Library) {
    this.libr.push(lib);
  }

  addCarel(carel: Carel) {
   this.carel.push(carel);
  }
  updateBook(index: number, bookss: Book) {
    this.books[index] = bookss;
    this.bookSelected.next(this.books.slice());
  }
  deleteBook(index: number) {
    this.books.splice(index, 1);
    this.bookSelected.next(this.books.slice());
  }
  addnewBook(bookss: Book) {
    this.books.push(bookss);
    this.bookSelected.next(this.books.slice());
  }

  setBook(bookss: Book[]) {
    this.books = bookss;
    this.bookSelected.next(this.books.slice());
  }

  setCarel(carel: Carel[]) {
    this.carel = carel;
    this.carelSelected.next(this.carel.slice());
  }

  setLib(lib: Library []) {
    this.libr = lib;
    this.LibSelected.next(this.libr.slice());
  }

  returnBookForm(name: string) {
    for (const l of this.books) {
        if (l.name ===  name) {
            l.quantity++;
        }
    }
    this.bookSelected.next(this.books.slice());
  }

  getBookfromIndex(index: number)  {
    return this.books[index];
  }
  updateBookNumber(index: number) {
    this.books[index].quantity--;
    this.bookSelected.next(this.books.slice());
  }


}
