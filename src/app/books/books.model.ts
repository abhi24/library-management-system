export class Book {
  public name: string;
  public cost: number;
  public image: string;
  public quantity: number;

  constructor(name: string, cost: number, image: string, quantity: number) {
    this.name = name;
    this.cost = cost;
    this.image = image;
    this.quantity = quantity;

  }
}
