import { Component, OnInit } from '@angular/core';
import {Book} from '../books.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BooksService} from '../books.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  book: Book;
  index: number;
  issueBook = false;
  bookisSelected = false;
  constructor(private booksService: BooksService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    console.log('hi');
    this.index = +this.route.params.subscribe(
      (params: Params) => {
        this.issueBook = false;
        this.index = +params['id'];
         this.book = this.booksService.getBookfromIndex(this.index);
         if (this.book.quantity === 0) {
           this.issueBook = true;
         }
      }
     );
  }

  onIssueBookPressed() {
    this.booksService.updateBookNumber(this.index);
  }
  onEditPressed() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }
  onDeletePressed() {
    this.booksService.deleteBook(this.index);
  }

}
