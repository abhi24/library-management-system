import { Component, OnInit } from '@angular/core';
import {Book} from '../books.model';
import {BooksService} from '../books.service';
import {ActivatedRoute, Router, Routes} from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  books: Book[];
  filteredStatus: '';
  bookisSelected = false;
  constructor(private bookSer: BooksService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.bookSer.bookSelected
      .subscribe(
        (books: Book[]) => {
          this.books = books;
        }
      );
    this.books = this.bookSer.getBooks();
  }

  newBookPressed() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
