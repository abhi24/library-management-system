import {Component, Input, OnInit} from '@angular/core';
import {Book} from '../../books.model';
import {BooksService} from '../../books.service';

@Component({
  selector: 'app-searchbook',
  templateUrl: './searchbook.component.html',
  styleUrls: ['./searchbook.component.css']
})
export class SearchbookComponent implements OnInit {
  @Input() book1: Book;

  constructor(private bookSer: BooksService) { }

  ngOnInit() {

  }
}
