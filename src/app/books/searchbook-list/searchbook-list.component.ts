import {Component, Input, OnInit, Output} from '@angular/core';
import {Book} from '../books.model';
import {BooksService} from '../books.service';

@Component({
  selector: 'app-searchbook-list',
  templateUrl: './searchbook-list.component.html',
  styleUrls: ['./searchbook-list.component.css']
})
export class SearchbookListComponent implements OnInit {

  bookss: Book[];

  constructor(private bookSer: BooksService) { }

  ngOnInit() {

    this.bookSer.bookSelected
      .subscribe(
        (books: Book[]) => {
          this.bookss = books;
        }
      );
    this.bookss = this.bookSer.getBooks();
    console.log(this.bookss);
  }

}
