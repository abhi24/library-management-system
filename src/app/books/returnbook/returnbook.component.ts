import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BooksService} from '../books.service';
import {Book} from '../books.model';

@Component({
  selector: 'app-returnbook',
  templateUrl: './returnbook.component.html',
  styleUrls: ['./returnbook.component.css']
})
export class ReturnbookComponent implements OnInit {
  book: Book[];
  bookForChecking: Book[];
  returnBookForm: FormGroup;
  bStatus = false;
  bookFound = false;
  bookStatus = false;
  constructor(private bookSer: BooksService) { }

  ngOnInit() {
    let bookName: '';
    this.returnBookForm = new FormGroup({
      'name': new FormControl(bookName, Validators.required),
    });
  }
  onSubmit() {
    this.bStatus = true;
    this.bookStatus = false;
    this.bookFound = false;
    let bn: string;
     bn = this.returnBookForm.value.name;
      this.bookForChecking = this.bookSer.getBooks();
      for (const l of this.bookForChecking) {
        if (l.name === bn ) {
          this.bookFound = true;
        }
      }
      if (this.bookFound) {
        console.log('Book Returned');
        this.bookSer.returnBookForm(bn);
        this.bookStatus = true;
      } else {
        console.log('Book Not Found');
        this.bookStatus = false;
      }
    }
}
