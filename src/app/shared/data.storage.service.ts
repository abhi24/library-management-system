import { Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {BooksService} from '../books/books.service';
import {Book} from '../books/books.model';
import {AuthService} from '../auth/auth.service';
import {Carel} from '../rooms/carel.model';
import {Library} from '../maps/library.module';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class DataStorageService {

  in: number;
   nameIn: string;

  constructor(private http: Http, private bookSer: BooksService, private authService: AuthService ) {
  }

  storeBooks() {
    const token = this.authService.getToken();
    const server = this.bookSer.getBooks();
    return this.http.put('https://abhi-http.firebaseio.com/book.json?auth=' + token, server);
  }

  addCarel1() {
    const token = this.authService.getToken();
    const carel = this.bookSer.getCarel();
    return this.http.put('https://abhi-http.firebaseio.com/carel.json?auth=' + token, carel);

  }

  addLib() {
    const token = this.authService.getToken();
    return this.http.put('https://abhi-http.firebaseio.com/library.json?auth=' + token , this.bookSer.getLibrary());
  }

  getCarel() {
    const token = this.authService.getToken();
    this.http.get('https://abhi-http.firebaseio.com/carel.json?auth=' + token)
      .subscribe(
        (response: Response) => {
          const carel: Carel[] = response.json();
          this.bookSer.setCarel(carel);
        }
      );
  }

  getBook() {
    const token = this.authService.getToken();
      this.http.get('https://abhi-http.firebaseio.com/book.json?auth=' + token)
        .subscribe(
          (response: Response) => {
            const book: Book[] = response.json();
            this.bookSer.setBook(book);
          }
        );
  }

  getlibraryData() {
    this.http.get('https://abhi-http.firebaseio.com/library.json')
      .subscribe(
        (response: Response) => {
          const libdata: Library[] = response.json();
          this.bookSer.setLib(libdata);
        }
      );
  }

}
