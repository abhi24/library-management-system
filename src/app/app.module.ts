import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {DropdownDirective} from './shared/dropdown.directive';
import { BooksComponent } from './books/books.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { CareerComponent } from './career/career.component';
import {AppRoutingModule} from './app-routing.module';
import { BookItemComponent } from './books/book-list/book-item/book-item.component';
import { NewbookComponent } from './books/newbook/newbook.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';
import { FilterPipe } from './filter.pipe';
import { SearchbookComponent } from './books/searchbook-list/searchbook/searchbook.component';
import { SearchbookListComponent } from './books/searchbook-list/searchbook-list.component';
import { ReturnbookComponent } from './books/returnbook/returnbook.component';
import { BookstartComponent } from './books/bookstart/bookstart.component';
import {DataStorageService} from './shared/data.storage.service';
import {BooksService} from './books/books.service';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import {AuthService} from './auth/auth.service';
import {AuthGuardService} from './auth/auth-guard.service';
import { StudyCarrelComponent } from './rooms/study-carrel/study-carrel.component';
import { CarelReservedComponent } from './rooms/carel-reserved/carel-reserved.component';
import {AgmCoreModule} from '@agm/core';
import { MapsComponent } from './maps/maps.component';
import {SearchLibComponent} from './maps/search-lib/search-lib.component';
import {TestPipe} from './about/test.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DropdownDirective,
    BooksComponent,
    BookListComponent,
    BookDetailsComponent,
    HomeComponent,
    AboutComponent,
    CareerComponent,
    BookItemComponent,
    NewbookComponent,
    BookEditComponent,
    FilterPipe,
    SearchbookComponent,
    SearchbookListComponent,
    ReturnbookComponent,
    BookstartComponent,
    SignInComponent,
    SignUpComponent,
    StudyCarrelComponent,
    CarelReservedComponent,
    MapsComponent,
    SearchLibComponent,
    TestPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCOn4RZ7yNHg0xysGXZN2tDVRnw-DjrldE',
      libraries: ['places']
    }),
  ],
  providers: [DataStorageService, BooksService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
