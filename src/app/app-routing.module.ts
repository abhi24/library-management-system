import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {CareerComponent} from './career/career.component';
import {AboutComponent} from './about/about.component';
import {NgModule} from '@angular/core';
import {BookDetailsComponent} from './books/book-details/book-details.component';
import {BooksComponent} from './books/books.component';
import {BookEditComponent} from './books/book-edit/book-edit.component';
import {SearchbookComponent} from './books/searchbook-list/searchbook/searchbook.component';
import {SearchbookListComponent} from './books/searchbook-list/searchbook-list.component';
import {ReturnbookComponent} from './books/returnbook/returnbook.component';
import {BookstartComponent} from './books/bookstart/bookstart.component';
import {SignUpComponent} from './auth/sign-up/sign-up.component';
import {SignInComponent} from './auth/sign-in/sign-in.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {StudyCarrelComponent} from './rooms/study-carrel/study-carrel.component';
import {CarelReservedComponent} from './rooms/carel-reserved/carel-reserved.component';
import {MapsComponent} from './maps/maps.component';
import {SearchLibComponent} from './maps/search-lib/search-lib.component';

const routings: Routes = [
  {path: '' , component: HomeComponent},
  {path: 'about' , component: AboutComponent},
  {path: 'career' , component: CareerComponent},

  {path: 'books' , component: BooksComponent, children: [
    {path: '' , component: BookstartComponent},
    {path: 'new' , component: BookEditComponent, canActivate: [AuthGuardService]},
    {path: ':id' , component: BookDetailsComponent},
    {path: ':id/edit' , component: BookEditComponent, canActivate: [AuthGuardService]},
  ]},
  {path: 'search' , component: SearchbookComponent},
  {path: 'searchlist' , component: SearchbookListComponent},
  {path: 'returnBook' , component: ReturnbookComponent},
  {path: 'signup' , component: SignUpComponent},
  {path: 'signin' , component: SignInComponent},
  {path: 'study-carrel' , component: StudyCarrelComponent},
  {path: 'carel-reserved' , component: CarelReservedComponent},
  {path: 'maps' , component: MapsComponent},
  {path: 'map-search' , component: SearchLibComponent},
  {path: 'about' , component: AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routings)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
