# README #

This app is developed using **Angular 4**
This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###
This repository is a library management system where users and admin can:
> ### User
> 
> 1.   Get Book Issues
> 2.   Return Books
> 3.   Reserve Study Carels with partners
> 4.   Search for libraries on Map
>
> ### Admin
>
> 1.   Add/Delete new book
> 2.   Issue Book
> 3.   Reserve Study Carels with partners


## Getting Started ##

### Running the Project

We're using Node.js, NPM (Node Package Manager). To preview code locally, you'll need to install Node and NPM, then run the following commands from a terminal window, in the repo directory:

> $ npm install
>
> $ ng serve --port 4040

Those commands do the following:

npm install will install the necessary node.js packages to develop on this project
Once run, you can preview the site by pointing a web server at the dist directory (see below).

ng serve will run the code on local host server 4040

### Downloading the project

Download the entire project and go to the run ng-serve from the folder 
terminal and you can see the project. To test the project enter the username: **abcdef1234@gmail.com** and password:**12345678** and then from login fetch all the data from the database first.

## Images
>
   **Main Screen**
>
![Alt text](https://bytebucket.org/abhi24/library-management-system/raw/65ebb13014232f8b0552666e6bb8f48d9d145f59/src/pictures/Screen%20Shot%202017-09-09%20at%209.09.29%20PM.png)

>
  **Books Page**
>
![Alt text](https://bytebucket.org/abhi24/library-management-system/raw/65ebb13014232f8b0552666e6bb8f48d9d145f59/src/pictures/Screen%20Shot%202017-09-09%20at%209.10.53%20PM.png)

>
   **Study Carel** 
>
![Alt text](https://bytebucket.org/abhi24/library-management-system/raw/65ebb13014232f8b0552666e6bb8f48d9d145f59/src/pictures/Screen%20Shot%202017-09-09%20at%209.11.44%20PM.png) 

>
   **Seach for Different Libraries in Google Maps**
>
![Alt text](https://bytebucket.org/abhi24/library-management-system/raw/65ebb13014232f8b0552666e6bb8f48d9d145f59/src/pictures/Screen%20Shot%202017-09-09%20at%209.13.58%20PM.png) 


## Who do I talk to? ###

>Abhishek Gupta
>
>guptaabhishek18154@gmail.com
>
>607-338-0607